import {Aircraft} from "./Aircraft";
import {AircraftKeys} from "../keys/AircraftKeys";


export class CivilAircraft extends Aircraft {
    private _gateLicense: boolean;
    private aircraftKeys: AircraftKeys = new AircraftKeys();

    get gateLicense(): boolean {
        return this._gateLicense;
    }
    set gateLicense(value: boolean) {
        this._gateLicense = value;
    }
    getCivilAircraft(): any {
        const array: object = {
            [this.aircraftKeys.id]: this.id,
            [this.aircraftKeys.name]: this.name,
            [this.aircraftKeys.distance]: this.distance,
            [this.aircraftKeys.seatCount]: this.seatCount,
            [this.aircraftKeys.maxSeatCount]: this.maxSeatCount,
            [this.aircraftKeys.length]: this.length,
            [this.aircraftKeys.wingSpan]: this.wingSpan,
            [this.aircraftKeys.height]: this.height,
            [this.aircraftKeys.consumption]: this.consumption,
            [this.aircraftKeys.gateLicense]: this._gateLicense
        };
        // tslint:disable-next-line:radix
        if (isNaN(parseInt(String(this.id)))) {
            delete array["id"];
        }
        return array;
    }

    toJSON(): object {
        return {
            ...super.toJSON(), ...{[this.aircraftKeys.gateLicense]: this.gateLicense}
        };
    }
}
