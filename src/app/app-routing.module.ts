import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./components/home/home.component";

import {AllAircraftComponent} from "./components/all-aircraft/all-aircraft.component";
import {CivilAircraftComponent} from "./components/civil-aircraft/civil-aircraft.component";
import {MilitaryAircraftComponent} from "./components/military-aircraft/military-aircraft.component";


const routes: Routes = [
    {path: "home", component: HomeComponent},
    {path: "all", component: AllAircraftComponent},
    {path: "civil", component: CivilAircraftComponent},
    {path: "military", component: MilitaryAircraftComponent},
    {path: "", component: HomeComponent},
    {path: "**", redirectTo: "/all"}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
    public static getPath(): any {
        return RouterModule.forRoot(routes);
    }
}
