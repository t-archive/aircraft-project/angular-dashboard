import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HomeComponent} from "./components/home/home.component";
import {AllAircraftComponent} from "./components/all-aircraft/all-aircraft.component";
import {CivilAircraftComponent} from "./components/civil-aircraft/civil-aircraft.component";
import {MilitaryAircraftComponent} from "./components/military-aircraft/military-aircraft.component";
import {FormsModule} from "@angular/forms";
import {SearchAircraftsComponent} from "./components/search-aircrafts/search-aircrafts.component";
import {EditModalComponent} from "./components/edit-modal/edit-modal.component";
import {
    MatTabsModule, MatCheckboxModule, MatInputModule, MatSelectModule,
    MatButtonModule, MatDialogModule, MatTooltipModule
} from "@angular/material";
import {BrowserAnimationsModule, NoopAnimationsModule} from "@angular/platform-browser/animations";
import {CreateModalComponent} from "./components/create-modal/create-modal.component";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AllAircraftComponent,
        CivilAircraftComponent,
        MilitaryAircraftComponent,
        SearchAircraftsComponent,
        EditModalComponent,
        CreateModalComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        MatDialogModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        BrowserModule,
        // import HttpClientModule after BrowserModule.
        HttpClientModule,
    ],
    exports: [
        MatDialogModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    entryComponents: [EditModalComponent, CreateModalComponent]
})
export class AppModule {
}
