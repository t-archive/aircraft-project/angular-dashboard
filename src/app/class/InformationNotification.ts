import Swal from "sweetalert2";
import {Aircraft} from "../aircrafts/Aircraft";
import {Dictionary} from "../keys/Dictionary";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";


export class InformationNotification {
    // private _dictionary: Dictionary;
    private static _dictionary: Dictionary = new Dictionary();

    constructor() {
    }

    public static Delete(aircraft: Aircraft): void {
        Swal.fire({
            toast: true,
            position: "top-end",
            icon: "warning",
            title: aircraft.name + " " + this._dictionary.wasDeleted,
            showConfirmButton: false,
            timer: 1500
        });
    }

    public static DeletedFailed(aircraft: Aircraft): void {
        Swal.fire({
            toast: true,
            position: "top-end",
            icon: "error",
            title: aircraft.name + " " + this._dictionary.wasNotDeletedDeletionError,
            showConfirmButton: false,
            timer: 1500
        });
    }

    public static Change(aircraft: IaircraftData): void {
        Swal.fire({
            toast: true,
            position: "top-end",
            icon: "warning",
            title: aircraft.name + " " + this._dictionary.wasEdited,
            showConfirmButton: false,
            timer: 1500
        });
    }

    public static ChangedFailed(aircraft: IaircraftData): void {
        Swal.fire({
            toast: true,
            position: "top-end",
            icon: "error",
            title: aircraft.name + " " + this._dictionary.wasNotEditedErrorWhileEditing,
            showConfirmButton: false,
            timer: 1500
        });
    }
    public static createFailed(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): void {
        Swal.fire({
            toast: true,
            position: "top-end",
            icon: "error",
            title: aircraft.name + " " + this._dictionary.createFailed,
            showConfirmButton: false,
            timer: 3000
        });
    }
    public static editFailed(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): void {
        Swal.fire({
            toast: true,
            position: "top-end",
            icon: "error",
            title: aircraft.name + " " + this._dictionary.editFailed,
            showConfirmButton: false,
            timer: 3000
        });
    }
    public static deleteFailed(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): void {
        Swal.fire({
            toast: true,
            position: "top-end",
            icon: "error",
            title: aircraft.name + " " + this._dictionary.deleteFailed,
            showConfirmButton: false,
            timer: 3000
        });
    }
    /**
     *
     * @param status
     * @param text
     * @param time
     */
    public static createToast(status: any, text: string, time: number = 1500): void {
        if (text) {
            if (status) {
                Swal.fire({
                    toast: true,
                    position: "top-end",
                    icon: status,
                    title: text,
                    showConfirmButton: false,
                    timer: time
                });
            } else {
                console.error("DEV: Missing '_status'");
            }

        } else {
            if (!status) {
                console.error("DEV: Missing '_text' and '_status'");
            } else {
                console.error("DEV: Missing '_text'");
            }

        }

    }
}
