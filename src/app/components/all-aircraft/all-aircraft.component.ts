import {Component, Inject, Input, OnInit} from "@angular/core";
import {Aircraft} from "../../aircrafts/Aircraft";
import {AircraftService} from "../../services/AircraftService";
import {DesignClass} from "../../class/DesignClass";
import {StoreService} from "../../services/store.service";
import {UnitExtension} from "../../keys/UnitExtension";
import {EditModalComponent} from "../edit-modal/edit-modal.component";
import {MatDialog} from "@angular/material";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {AircraftRepository} from "../../repository/AircraftRepository";
import {Dictionary} from "../../keys/Dictionary";
import {CreateModalComponent} from "../create-modal/create-modal.component";


@Component({
    selector: "app-all-aircraft",
    templateUrl: "./all-aircraft.component.html",
    styleUrls: ["./all-aircraft.component.css"]
})
export class AllAircraftComponent implements OnInit {
    private _allAircrafts: Array<Aircraft>;
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _designClass: DesignClass = new DesignClass();

    constructor(@Inject(AircraftService) private _aircraftService: AircraftService,
                @Inject(StoreService) private store: StoreService,
                @Inject(MatDialog) private dialog: MatDialog) {

        this._aircraftService.allAircraftsBehaviorSubject.subscribe(aircraftRepository => {
            if (aircraftRepository instanceof AircraftRepository) {
                this._allAircrafts = aircraftRepository.aircrafts;
            }
        });
    }

    ngOnInit(): void {
        this.store.filterTextBehaviorSubject.subscribe(message => {
            this._aircraftService.allAircraftsFilter(message);
        });
    }

    public openEditModal(aircraft: Aircraft): void {
        const dialogRef: any = this.dialog.open(EditModalComponent, {
            data: aircraft,
            autoFocus: false
        });

    }
    public openCreateModal(): void {
        const dialogRef: any = this.dialog.open(CreateModalComponent, {
            data: {
                behaviorSubject: this._aircraftService.allAircraftsBehaviorSubject,
                type: this.aircraftKeys.all
            },
            autoFocus: false
        });
    }

    sort(key: string): void {
        this._aircraftService.allAircraftsSort(key);
    }

    public delete(id: number): void {
        this._aircraftService.allAircraftDelete(id);
    }


    get allAircrafts(): Array<Aircraft> {
        return this._allAircrafts;
    }

    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }
    get designClass(): DesignClass {
        return this._designClass;
    }
    get dictionary(): Dictionary {
        return this._dictionary;
    }
}

