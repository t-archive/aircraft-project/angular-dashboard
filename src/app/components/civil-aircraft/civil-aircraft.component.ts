import {Component, Inject, OnInit} from "@angular/core";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {AircraftService} from "../../services/AircraftService";
import {DesignClass} from "../../class/DesignClass";
import {StoreService} from "../../services/store.service";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Aircraft} from "../../aircrafts/Aircraft";
import {EditModalComponent} from "../edit-modal/edit-modal.component";
import {MatDialog} from "@angular/material";
import {Dictionary} from "../../keys/Dictionary";
import {CreateModalComponent} from "../create-modal/create-modal.component";

@Component({
    selector: "app-civil-aircraft",
    templateUrl: "./civil-aircraft.component.html",
    styleUrls: ["./civil-aircraft.component.css"]
})
export class CivilAircraftComponent implements OnInit {



    private readonly _civilAircrafts: Array<CivilAircraft>;
    private _designClass: DesignClass = new DesignClass();
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _dictionary: Dictionary = new Dictionary();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();

    constructor(@Inject(AircraftService) private _aircraftService: AircraftService,
                @Inject(StoreService) private store: StoreService,
                @Inject(MatDialog) private dialog: MatDialog) {
        this._civilAircrafts = this._aircraftService.civilAircrafts;
    }

    ngOnInit(): void {
        this.store.filterTextBehaviorSubject.subscribe(message => {
            this._aircraftService.civilAircraftsFilter(message);
        });
    }
    public openEditModal(aircraft: Aircraft): void {
        // aircraft.name = aircraft.name + "TEST";
        const dialogRef: any = this.dialog.open(EditModalComponent, {
            data: aircraft,
            autoFocus: false
        });
    }
    public openCreateModal(): void {
        const dialogRef: any = this.dialog.open(CreateModalComponent, {
            data: {
                behaviorSubject: this._aircraftService.civilAircraftsBehaviorSubject,
                type: this.aircraftKeys.civil,
            },
            autoFocus: false
        });
    }
    sort(key: string): void {
        this._aircraftService.civilAircraftsSort(key);
    }
    public delete(id: number): void {
        this._aircraftService.civilAircraftDelete(id);
    }

    get civilAircrafts(): Array<CivilAircraft> {
        return this._civilAircrafts;
    }
    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }
    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }
    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }
    get designClass(): DesignClass {
        return this._designClass;
    }
    get dictionary(): Dictionary {
        return this._dictionary;
    }
}
