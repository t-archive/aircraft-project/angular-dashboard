import {Component, Inject, OnInit, Output} from "@angular/core";
import {MilitaryAircraft} from "../../aircrafts/MilitaryAircraft";
import {AircraftService} from "../../services/AircraftService";
import {DesignClass} from "../../class/DesignClass";
import {StoreService} from "../../services/store.service";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Aircraft} from "../../aircrafts/Aircraft";
import {EditModalComponent} from "../edit-modal/edit-modal.component";
import {MatDialog} from "@angular/material";
import {CreateModalComponent} from "../create-modal/create-modal.component";
import {Dictionary} from "../../keys/Dictionary";

@Component({
    selector: "app-military-aircraft",
    templateUrl: "./military-aircraft.component.html",
    styleUrls: ["./military-aircraft.component.css"]
})
export class MilitaryAircraftComponent implements OnInit {


    private readonly _militaryAircrafts: Array<MilitaryAircraft>;
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _designClass: DesignClass = new DesignClass();

    constructor(@Inject(AircraftService) private _aircraftService: AircraftService,
                @Inject(StoreService) private store: StoreService,
                @Inject(MatDialog) private dialog: MatDialog) {
        this._militaryAircrafts = this._aircraftService.militaryAircrafts;

    }

    ngOnInit(): void {
        this.store.filterTextBehaviorSubject.subscribe(message => {
            this._aircraftService.militaryAircraftsFilter(message);
        });
    }

    sort(key: string): void {
        this._aircraftService.militaryAircraftsSort(key);
    }
    public openEditModal(aircraft: Aircraft): void {
        const dialogRef: any = this.dialog.open(EditModalComponent, {
            data: aircraft,
            autoFocus: false
        });
    }
    public openCreateModal(): void {
        const dialogRef: any = this.dialog.open(CreateModalComponent, {
            data: {
                behaviorSubject: this._aircraftService.militaryAircraftsBehaviorSubject,
                type: this.aircraftKeys.military,
            },
            autoFocus: false
        });
    }
    public delete(id: number): void {
        this._aircraftService.militaryAircraftDelete(id);
    }

    get militaryAircrafts(): Array<MilitaryAircraft> {
        return this._militaryAircrafts;
    }
    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }
    get designClass(): DesignClass {
        return this._designClass;
    }
    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }
    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }
    get dictionary(): Dictionary {
        return this._dictionary;
    }
}
