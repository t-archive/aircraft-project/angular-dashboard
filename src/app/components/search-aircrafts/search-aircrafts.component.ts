import {Component, EventEmitter, Inject, Input, OnInit, Output} from "@angular/core";
import {Subject} from "rxjs";
import {debounceTime} from "rxjs/operators";
import {StoreService} from "../../services/store.service";
import {Dictionary} from "../../keys/Dictionary";

@Component({
    selector: "app-search-aircrafts",
    templateUrl: "./search-aircrafts.component.html",
    styleUrls: ["./search-aircrafts.component.css"]
})
export class SearchAircraftsComponent implements OnInit {

    private _searchInput: string;
    private _dictionary: Dictionary = new Dictionary();
    @Output() inputSearchEvent: any = new EventEmitter<string>();
    private _debouncer: Subject<string> = new Subject<string>();

    constructor() { // @Inject(StoreService) private store: StoreService
        this._searchInput = "";
        this._debouncer.pipe(debounceTime(400))
            .subscribe((value) => {
                this.inputSearchEvent.emit(value);
            });
    }

    ngOnInit(): void {
    }

    get searchInput(): string {
        return this._searchInput;
    }

    set searchInput(value: string) {
        this._searchInput = value.trim();
        this._debouncer.next(this.searchInput);
    }

}
