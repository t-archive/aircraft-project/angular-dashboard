interface IaircraftData {
    id: number;
    name: string;
    distance: number;
    seatCount: number;
    maxSeatCount: number;
    length: number;
    wingSpan: number;
    height: number;
    consumption: number;
}
