export const DATA_KEY: string = "data";
export const ALL_KEY: string = "all";
export const CIVIL_KEY: string = "civil";
export const MILITARY_KEY: string = "military";
export const AIRCRAFTS_KEY: string = "aircrafts";
export const ID_KEY: string = "id";
export const NAME_KEY: string = "name";
export const DISTANCE_KEY: string = "distance";
export const SEATCOUNT_KEY: string = "seatCount";
export const MAXSEATCOUNT_KEY: string = "maxSeatCount";
export const LENGTH_KEY: string = "length";
export const WINGSPAN_KEY: string = "wingSpan";
export const HEIGHT_KEY: string = "height";
export const CONSUMPTION_KEY: string = "consumption";
export const AIRREFUELING_KEY: string = "airRefueling";
export const FLARE_KEY: string = "flare";
export const GATELICENSE_KEY: string = "gateLicense";
export const JSON_AIRCRAFT_KEY: string = "aircraft";

export class AircraftKeys {


    private _data: string = DATA_KEY;
    private _all: string = ALL_KEY;
    private _civil: string = CIVIL_KEY;
    private _military: string = MILITARY_KEY;
    private _aircrafts: string = AIRCRAFTS_KEY;
    private _id: string = ID_KEY;
    private _name: string = NAME_KEY;
    private _distance: string = DISTANCE_KEY;
    private _seatCount: string = SEATCOUNT_KEY;
    private _maxSeatCount: string = MAXSEATCOUNT_KEY;
    private _length: string = LENGTH_KEY;
    private _wingSpan: string = WINGSPAN_KEY;
    private _height: string = HEIGHT_KEY;
    private _consumption: string = CONSUMPTION_KEY;
    private _airRefueling: string = AIRREFUELING_KEY;
    private _flare: string = FLARE_KEY;
    private _gateLicense: string = GATELICENSE_KEY;
    private _jsonAircraft: string = JSON_AIRCRAFT_KEY;

    constructor() {
    }

    get jsonAircraft(): string {
        return this._jsonAircraft;
    }

    get all(): string {
        return this._all;
    }

    get gateLicense(): string {
        return this._gateLicense;
    }

    get flare(): string {
        return this._flare;
    }

    get airRefueling(): string {
        return this._airRefueling;
    }

    get consumption(): string {
        return this._consumption;
    }

    get height(): string {
        return this._height;
    }

    get wingSpan(): string {
        return this._wingSpan;
    }

    get length(): string {
        return this._length;
    }

    get maxSeatCount(): string {
        return this._maxSeatCount;
    }

    get seatCount(): string {
        return this._seatCount;
    }

    get distance(): string {
        return this._distance;
    }

    get name(): string {
        return this._name;
    }

    get id(): string {
        return this._id;
    }

    get aircrafts(): string {
        return this._aircrafts;
    }

    get military(): string {
        return this._military;
    }

    get civil(): string {
        return this._civil;
    }

    get data(): string {
        return this._data;
    }
}
