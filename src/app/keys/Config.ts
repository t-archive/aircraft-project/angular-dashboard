export const HTTP_BASE_URL: string = "";

export class Config {

    private _http_base_url: string = HTTP_BASE_URL;

    get http_base_url(): string {
        return this._http_base_url;
    }
}
