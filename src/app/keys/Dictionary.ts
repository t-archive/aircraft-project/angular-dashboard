export class Dictionary {


    private _saveChange: string = "Speichern";
    private _cancel: string = "Schließen";
    private _homePage: string = "Startseite";
    private _allAircrafts: string = "Alle Flugzeuge";
    private _civilAircrafts: string = "Zivile Flugzeuge";
    private _militaryAircrafts: string = "Militärflugzeuge";
    private _aircrafts: string = "Flugzeuge";
    private _aircraft: string = "Flugzeug";
    private _search: string = "Suche";
    private _create: string = "Erstellen";
    private _edit: string = "Bearbeiten";
    private _delete: string = "Löschen";
    private _yes: string = "Ja";
    private _no: string = "Nein";
    private _pleaseCloseThisWindowModal: string = "Bitte schließen Sie dieses Fenster";
    private _wasDeleted: string = "wurde Gelöscht!";
    private _wasNotDeletedDeletionError: string = "wurde nicht Gelöscht! Fehler beim Löschen!";
    private _wasEdited: string = "wurde Bearbeitet!";
    private _wasNotEditedErrorWhileEditing: string = "wurde nicht Bearbeitet! Fehler beim Bearbeiten!";
    private _aircraftTypeSelect: string = "Flugzeug Typ";
    private _pleaseSelect: string = "Bitte auswählen";
    private _createFailed: string = "Konnte nicht Erstellt werden!";
    private _editFailed: string = "Konnte nicht Bearbeitet werden!";
    private _deleteFailed: string = "Konnte nicht Gelöscht werden!";

    get deleteFailed(): string {
        return this._deleteFailed;
    }

    get editFailed(): string {
        return this._editFailed;
    }

    get pleaseSelect(): string {
        return this._pleaseSelect;
    }

    get aircraftTypeSelect(): string {
        return this._aircraftTypeSelect;
    }

    get create(): string {
        return this._create;
    }

    get wasNotEditedErrorWhileEditing(): string {
        return this._wasNotEditedErrorWhileEditing;
    }

    get wasEdited(): string {
        return this._wasEdited;
    }

    get wasNotDeletedDeletionError(): string {
        return this._wasNotDeletedDeletionError;
    }

    get wasDeleted(): string {
        return this._wasDeleted;
    }

    get cancel(): string {
        return this._cancel;
    }

    get pleaseCloseThisWindowModal(): string {
        return this._pleaseCloseThisWindowModal;
    }

    get no(): string {
        return this._no;
    }

    get yes(): string {
        return this._yes;
    }

    get delete(): string {
        return this._delete;
    }

    get edit(): string {
        return this._edit;
    }

    get search(): string {
        return this._search;
    }

    get aircraft(): string {
        return this._aircraft;
    }

    get aircrafts(): string {
        return this._aircrafts;
    }

    get militaryAircrafts(): string {
        return this._militaryAircrafts;
    }

    get civilAircrafts(): string {
        return this._civilAircrafts;
    }

    get allAircrafts(): string {
        return this._allAircrafts;
    }

    get homePage(): string {
        return this._homePage;
    }

    get saveChange(): string {
        return this._saveChange;
    }

    get createFailed(): string {
        return this._createFailed;
    }
}
