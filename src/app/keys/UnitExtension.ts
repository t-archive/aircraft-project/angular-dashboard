export class UnitExtension {
    private _name: string = "";
    private _distance: string = "km";
    private _seatCount: string = "PAX";
    private _maxSeatCount: string = "PAX";
    private _length: string = "m";
    private _wingSpan: string = "m";
    private _height: string = "m";
    private _consumption: string = "l/100 km";

    get consumption(): string {
        return this._consumption;
    }
    get height(): string {
        return this._height;
    }
    get wingSpan(): string {
        return this._wingSpan;
    }
    get length(): string {
        return this._length;
    }
    get maxSeatCount(): string {
        return this._maxSeatCount;
    }
    get seatCount(): string {
        return this._seatCount;
    }
    get distance(): string {
        return this._distance;
    }
    get name(): string {
        return this._name;
    }
}
