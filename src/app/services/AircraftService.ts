import {Inject, Injectable} from "@angular/core";
import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {AircraftFactory} from "../factories/AircraftFactory";
import * as CivilAircraftJson from "../../assets/json/civilAircrafts.json";
import * as MilitaryAircraftJson from "../../assets/json/militaryAircrafts.json";
import * as AllAircraftJson from "../../assets/json/allAircrafts.json";
import {AircraftRepository} from "../repository/AircraftRepository";
import {BehaviorSubject} from "rxjs";
import {DOCUMENT} from "@angular/common";
import {HTTPService} from "./http.service";
import {InformationNotification} from "../class/InformationNotification";

@Injectable({
    providedIn: "root"
})
export class AircraftService {


    private _allAircrafts: BehaviorSubject<AircraftRepository>;
    private _civilAircrafts: BehaviorSubject<AircraftRepository>;
    private _militaryAircrafts: BehaviorSubject<AircraftRepository>;


    constructor(@Inject(HTTPService) private httpService: HTTPService) {
        this._allAircrafts = new BehaviorSubject<AircraftRepository>(new AircraftRepository());
        this._civilAircrafts = new BehaviorSubject<AircraftRepository>(new AircraftRepository());
        this._militaryAircrafts = new BehaviorSubject<AircraftRepository>(new AircraftRepository());
        this.start();
    }

    public start(): void {
        this.httpService.getAllAircraft().subscribe(data => {
            if (data.body) {
                const aircrafts: Array<Aircraft> = AircraftFactory.createAllAircrafts(data.body);
                for (const aircraft of aircrafts) {
                    this._allAircrafts.getValue().add(aircraft);
                }
            } else {
                InformationNotification.createToast("error", "No Data in Request!", 10000);
            }
        }, error => {
            InformationNotification.createToast("error", "No Connection!", 10000);
        });
        this.httpService.getCivilAircraft().subscribe(data => {
            if (data.body) {
                const aircrafts: Array<Aircraft> = AircraftFactory.createCivilAircrafts(data.body);
                for (const aircraft of aircrafts) {
                    this._civilAircrafts.getValue().add(aircraft);
                }
            } else {
                InformationNotification.createToast("error", "No Data in Request!", 10000);
            }
        }, error => {
            InformationNotification.createToast("error", "No Connection!", 10000);
        });

        this.httpService.getMilitaryAircraft().subscribe(data => {
            if (data.body) {
                const aircrafts: Array<Aircraft> = AircraftFactory.createMilitaryAircrafts(data.body);
                for (const aircraft of aircrafts) {
                    this._militaryAircrafts.getValue().add(aircraft);
                }
            } else {
                InformationNotification.createToast("error", "No Data in Request!", 10000);
            }
        }, error => {
            InformationNotification.createToast("error", "No Connection!", 10000);
        });
    }


    public allAircraftDelete(id: number): void {
        const aircraft: Aircraft = this._allAircrafts.getValue().getAircraftByID(id);

        this.httpService.deleteAircraft(aircraft).subscribe(
            (success) => {
                const status: boolean = this.httpService.checkIfNoErrorDelete(success);
                if (status === true) {
                    this._allAircrafts.getValue().remove(aircraft);
                    if (aircraft instanceof CivilAircraft) {
                        this._civilAircrafts.getValue().remove(this._civilAircrafts.getValue().getAircraftByID(id));
                    } else if (aircraft instanceof MilitaryAircraft) {
                        this._militaryAircrafts.getValue().remove(this._militaryAircrafts.getValue().getAircraftByID(id));
                    }

                } else {
                    InformationNotification.deleteFailed(aircraft);
                }
            },
            error => console.error(error)
        );
    }

    public civilAircraftDelete(id: number): void {
        const aircraft: Aircraft = this._civilAircrafts.getValue().getAircraftByID(id);

        this.httpService.deleteAircraft(aircraft).subscribe(
            (success) => {
                const status: boolean = this.httpService.checkIfNoErrorDelete(success);
                if (status === true) {
                    this._civilAircrafts.getValue().remove(aircraft);
                    this._allAircrafts.getValue().remove(this._allAircrafts.getValue().getAircraftByID(id));
                } else {
                    InformationNotification.deleteFailed(aircraft);
                }
            },
            error => console.error(error)
        );

    }

    public militaryAircraftDelete(id: number): void {
        const aircraft: Aircraft = this._militaryAircrafts.getValue().getAircraftByID(id);

        this.httpService.deleteAircraft(aircraft).subscribe(
            (success) => {
                const status: boolean = this.httpService.checkIfNoErrorDelete(success);
                if (status === true) {
                    this._militaryAircrafts.getValue().remove(aircraft);
                    this._allAircrafts.getValue().remove(this._allAircrafts.getValue().getAircraftByID(id));
                } else {
                    InformationNotification.deleteFailed(aircraft);
                }
            },
            error => console.error(error)
        );
    }


    public civilAircraftEdit(oldAircraft: Aircraft, newAircraft: IcivilAircraftData): CivilAircraft {
        this._allAircrafts.getValue().changeCivil(oldAircraft as CivilAircraft, newAircraft);
        return this._civilAircrafts.getValue().changeCivil(oldAircraft as CivilAircraft, newAircraft);
    }

    public militaryAircraftEdit(oldAircraft: Aircraft, newAircraft: ImilitaryAircraftData): MilitaryAircraft {
        this._allAircrafts.getValue().changeMilitary(oldAircraft as MilitaryAircraft, newAircraft);
        return this._militaryAircrafts.getValue().changeMilitary(oldAircraft as MilitaryAircraft, newAircraft);
    }

    get civilAircrafts(): Array<CivilAircraft> {
        return this._civilAircrafts.getValue().aircrafts;
    }

    get militaryAircrafts(): Array<MilitaryAircraft> {
        return this._militaryAircrafts.getValue().aircrafts;
    }

    get militaryAircraftsBehaviorSubject(): BehaviorSubject<AircraftRepository> {
        return this._militaryAircrafts;
    }

    get civilAircraftsBehaviorSubject(): BehaviorSubject<AircraftRepository> {
        return this._civilAircrafts;
    }

    get allAircraftsBehaviorSubject(): BehaviorSubject<AircraftRepository> {
        return this._allAircrafts;
    }

    set militaryAircraftsBehaviorSubject(value: BehaviorSubject<AircraftRepository>) {
        this._militaryAircrafts = value;
    }

    set civilAircraftsBehaviorSubject(value: BehaviorSubject<AircraftRepository>) {
        this._civilAircrafts = value;
    }

    set allAircraftsBehaviorSubject(value: BehaviorSubject<AircraftRepository>) {
        this._allAircrafts = value;
    }

    public allAircraftsFilter(search: string): Array<Aircraft> {
        return this._allAircrafts.getValue().filter(search);
    }

    public civilAircraftsFilter(search: string): Array<Aircraft> {
        return this._civilAircrafts.getValue().filter(search);
    }

    public militaryAircraftsFilter(search: string): Array<Aircraft> {
        return this._militaryAircrafts.getValue().filter(search);
    }

    public allAircraftsSort(key: string): any {
        return this._allAircrafts.getValue().sortByKey(key);
    }

    public civilAircraftsSort(key: string): any {
        return this._civilAircrafts.getValue().sortByKey(key);
    }

    public militaryAircraftsSort(key: string): any {
        return this._militaryAircrafts.getValue().sortByKey(key);
    }
}
