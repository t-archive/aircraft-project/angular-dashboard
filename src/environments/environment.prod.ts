const baseApiEndpoint: string = "http://api.carobserver.de/aircraft/index.php";

export const environment: any = {
    production: true,
    allAircraftURL: baseApiEndpoint + "/aircrafts",
    civilAircraftURL: baseApiEndpoint + "/aircrafts/civil",
    militaryAircraftURL: baseApiEndpoint + "/aircrafts/military",
    deleteAircraftURL: baseApiEndpoint + "/aircrafts/id/"
};
