// @ts-ignore
import {enableProdMode} from "@angular/core";
// @ts-ignore
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";

import {AppModule} from "./app/app.module";
// @ts-ignore
import {environment} from "./environments/environment";
import {AllAircraftComponent} from "./app/components/all-aircraft/all-aircraft.component";
import {CivilAircraftComponent} from "./app/components/civil-aircraft/civil-aircraft.component";
import {MilitaryAircraftComponent} from "./app/components/military-aircraft/military-aircraft.component";

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

